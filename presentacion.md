# Contexto

## Eventos

Una gran cantidad de eventos en todo el mundo se han visto beneficiados debido a
las redes sociales que logran un nivel de difusión que permite crear eventos
masivos en solo unos momentos.


## Crowdfunding 

El crowdfunding o financiación masiva ha permitido que personas puedan llevar a
cabo proyectos que de otra forma no podrían realizarse o conseguir dinero para
obras benéficas.

\begin{figure}[t]

	\centering
	\includegraphics[scale=0.3]{crowdfunding_money.jpg}
	\caption{El crowdfunding}

\end{figure}

<!---


--->



# Problema
## Problema

Falta de una plataforma que gestione los eventos desde el punto de vista de los
recursos, auspiciadores y dinero.


<!---

Kickstarter es una plataforma de crowdfunding pero solamente gestiona el dinero,
no los recursos y auspiciadores.

--->



# Solución
## Solución

Una plataforma web que permita publicar eventos para que estos sean financiados
de forma anticipada y que además ofresca auspiciadores cuando un evento cumpla
con los requisitos de un auspiciador.



## Modelo de negocios


\begin{figure}[h!]

    \centerline{
        \includegraphics[scale=0.6]{modelos/imagenes/modelo_negocios.png}
    }

\end{figure}


# Alcances y limitaciones
## Alcances y limitaciones

\begin{itemize}
    \item Mantener control de los recursos de un evento que se realizan en
        distintos lugares.
    \item Se realizan envío de notificaciones a través correo electrónico.
    \item La vinculación de productos de auspiciadores se realizará de forma
        automática a un evento que cumpla las condiciones.
    \item  Mantener control del estado del evento.
\end{itemize}



## Limitaciones



\begin{itemize}
    \item Solo eventos chilenos.
    \item El sistema no maneja transferencias de dinero ni pagos.
    \item No se hace manejo del stock de los proveedores ni auspiciadores.
    \item El precio de los recursos es fijado por el proveedor y no puede ser
        negociado a través del sistema.
    \item No existe prioridad entre los proveedores y auspiciadores al momento
        de ser listados en el sistema.
\end{itemize}








# Requerimientos
## Requerimientos

Se separan los requerimientos según el método MoSCoW

<!---


M: MUST, que se deben hacer para considerar la solución como éxito
S: SHOULD, requisitos de alta prioridad que deberían ir en la solución 
C: COULD, requisitos que se consideran deseables pero que no son necesarios
W: WONT, requisitos que pueden ser considerados para el futuro

--->


## Must

\begin{itemize}
    \item Registrar usuarios.
    \item Inscribir eventos.
    \item Escoger recursos para el evento.
    \item Definir tipos de financiamiento.
    \item Publicar eventos.
    \item Proveedor puede ofrecer recursos y lugares.
    \item El auspiciador ofrece productos a eventos que cumplan con condiciones.
\end{itemize}


## Should

\begin{itemize}
    \item Envío de notificaciones del estado del evento.
    \item Proveedores y auspiciadores pueden ver la información de los eventos
        en los que participan.
    \item El administrador puede agregar nuevas clases de recursos, condiciones
        para auspiciadores y tipos de eventos.
\end{itemize}


## Could

\begin{itemize}
    \item El auspiciador debe poder rechazar los eventos asignados
    automáticamente.
    \item Permitir agregar lugares con ayuda de google maps.
\end{itemize}







# Arquitectura preliminar
## Arquitectura preliminar


\begin{figure}[h!]

    \centerline{
        \includegraphics[scale=0.6]{modelos/imagenes/arquitectura.png}
    }

\end{figure}



# Modelo de operaciones
## Modelo de operaciones

\begin{figure}[h!]

    \centerline{
        \includegraphics[scale=0.6]{modelos/imagenes/modelo_general_operaciones.png}
    }

\end{figure}


# Actores y casos de uso
## Actores y casos de uso

\begin{figure}[h!]

    \centerline{
        \includegraphics[scale=0.35]{modelos/imagenes/casos_de_uso.png}
    }

\end{figure}


# Modelo general de procesos
## Nivel 0


\begin{figure}[h!]

    \centerline{
        \includegraphics[scale=0.5]{modelos/imagenes/DFD0.png}
    }

\end{figure}


# Modelo conceptual
## Modelo conceptual


\begin{figure}[h!]

    \centerline{
        \includegraphics[scale=0.4]{modelos/imagenes/modelo_datos_reducido.png}
    }

\end{figure}
